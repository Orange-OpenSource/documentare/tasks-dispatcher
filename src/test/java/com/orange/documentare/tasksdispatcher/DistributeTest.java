package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.Rule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Distribute test")
class DistributeTest {
    @Rule
    MockWebServer server = new MockWebServer();
    private List<String> urlsPaths = Collections.singletonList(server.url("/").toString());

    @Test
    @DisplayName("Receive a task id and then a result")
    void receive_task_id_and_then_a_result() {
        // given
        server.enqueue(new MockResponse().setBody("{\"id\": \"1234\"}"));
        server.enqueue(new MockResponse().setBody("{\"result\": \"I am a result\"}"));

        MyApiWrapper myApiWrapper = new MyApiWrapper();
        List<MyApiWrapper> apiWrappers = Collections.singletonList(myApiWrapper);

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = MyApiWrapper::setResult;
        (new TasksDispatcher(apiWrappers, urlsPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(myApiWrapper.result().result).isEqualTo("I am a result");
    }

    @Test
    @DisplayName("Receive a task id and then result is not ready and then the result")
    void receive_task_id_and_then_result_not_ready_and_then_the_result() {
        // given
        server.enqueue(new MockResponse().setBody("{\"id\": \"1234\"}"));
        server.enqueue(new MockResponse().setResponseCode(204));
        server.enqueue(new MockResponse().setBody("{\"result\": \"I am a result\"}"));

        MyApiWrapper myApiWrapper = new MyApiWrapper();
        List<MyApiWrapper> apiWrappers = asList(myApiWrapper);

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, urlsPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(myApiWrapper.result().result).isEqualTo("I am a result");
    }

    @Test
    @DisplayName("Receive 503 errors (server1 not available), then receive a task id and then a result")
    void receive_503_errors_then_receive_task_id_and_then_a_result() {
        // given
        server.enqueue(new MockResponse().setResponseCode(503));
        server.enqueue(new MockResponse().setResponseCode(503));
        server.enqueue(new MockResponse().setBody("{\"id\": \"1234\"}"));
        server.enqueue(new MockResponse().setBody("{\"result\": \"I am a result\"}"));

        MyApiWrapper myApiWrapper = new MyApiWrapper();
        List<MyApiWrapper> apiWrappers = asList(myApiWrapper);

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, urlsPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(myApiWrapper.result().result).isEqualTo("I am a result");
    }

    @Test
    @DisplayName("Receive a task id, then 503 error while retrieving result, then redistribute task")
    void receive_a_task_id_then_503_error_while_retrieving_result_then_redistribute_task() {
        // given
        server.enqueue(new MockResponse().setBody("{\"id\": \"1234\"}"));
        server.enqueue(new MockResponse().setResponseCode(503));
        server.enqueue(new MockResponse().setBody("{\"id\": \"5678\"}"));
        server.enqueue(new MockResponse().setBody("{\"result\": \"I am a result\"}"));

        MyApiWrapper myApiWrapper = new MyApiWrapper();
        List<MyApiWrapper> apiWrappers = asList(myApiWrapper);

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, urlsPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(myApiWrapper.result().result).isEqualTo("I am a result");
    }

    @Test
    @DisplayName("Receive two results")
    void receive_two_results() {
        // given
        int count = 1;
        List<MyApiWrapper> apiWrappers = new ArrayList<>();

        IntStream.range(0, count).forEach(index -> {
            server.enqueue(new MockResponse().setResponseCode(503));
            server.enqueue(new MockResponse().setBody(
                String.format("{\"id\": \"1234-%s\"}", index))
            );
            server.enqueue(new MockResponse().setBody(
                String.format("{\"result\": \"result %s\"}", index)
            ));
            apiWrappers.add(new MyApiWrapper());
        });

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, urlsPaths, resultObserver))
            .distribute()
            .block();

        // then
        IntStream.range(0, count).forEach(index ->
            assertThat(apiWrappers.get(index).result().result).isEqualTo("result " + index)
        );
    }
}
