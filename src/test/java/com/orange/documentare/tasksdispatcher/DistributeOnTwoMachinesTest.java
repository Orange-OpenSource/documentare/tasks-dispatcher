package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.Rule;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.BiConsumer;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Distribute test")
class DistributeOnTwoMachinesTest {
    @Rule
    MockWebServer server1 = new MockWebServer();

    @Rule
    MockWebServer server2 = new MockWebServer();

    @Test
    @DisplayName("Receive a task id and then a result on two machines")
    void receive_task_id_and_then_a_result_on_two_machines() {
        // given
        enqueueServerResponse(server1, "{\"id\": \"1234\"}");
        enqueueServerResponse(server1, "{\"result\": \"I am a result from server 1\"}");
        enqueueServerResponse(server2, "{\"id\": \"2345\"}");
        enqueueServerResponse(server2, "{\"result\": \"I am a result from server 2\"}");

        List<String> serversPaths = asList(server1.url("/").toString(), server2.url("/").toString());

        List<MyApiWrapper> apiWrappers = asList(new MyApiWrapper(), new MyApiWrapper());

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, serversPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(apiWrappers.stream().map(apiWrapper -> apiWrapper.result().result)).contains(
            "I am a result from server 1",
            "I am a result from server 2"
        );
    }

    private static void enqueueServerResponse(MockWebServer server, String responseBody) {
        server.enqueue(new MockResponse().setBody(responseBody));
    }

    @Test
    @DisplayName("Receive a task id, then 503 or 400 errors from this machine while retrieving result, then redistribute task on second machine")
    void receive_a_task_id_then_503_error_while_retrieving_result_then_redistribute_task() {
        // given
        enqueueServerResponse(server1, "{\"id\": \"1234\"}");
        server1.enqueue(new MockResponse().setResponseCode(400));
        server1.enqueue(new MockResponse().setResponseCode(503));
        server1.enqueue(new MockResponse().setResponseCode(404));
        enqueueServerResponse(server2, "{\"id\": \"2345\"}");
        enqueueServerResponse(server2, "{\"result\": \"I am a result from server 2\"}");


        List<String> serversPaths = asList(server1.url("/").toString(), server2.url("/").toString());

        List<MyApiWrapper> apiWrappers = asList(new MyApiWrapper());

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, serversPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(apiWrappers.stream().map(apiWrapper -> apiWrapper.result().result)).containsExactly(
            "I am a result from server 2"
        );
    }

    @Test
    @DisplayName("First machine should provide both results if second machine is not working")
    void first_machine_should_provide_both_results_if_second_machine_is_not_working() {
        // given
        enqueueServerResponse(server1, "{\"id\": \"1234\"}");
        enqueueServerResponse(server1, "{\"result\": \"I am first result from server 1\"}");
        enqueueServerResponse(server1, "{\"id\": \"5678\"}");
        enqueueServerResponse(server1, "{\"result\": \"I am second result from server 1\"}");
        server2.enqueue(new MockResponse().setResponseCode(500));

        List<String> serversPaths = asList(server1.url("/").toString(), server2.url("/").toString());

        List<MyApiWrapper> apiWrappers = asList(new MyApiWrapper(), new MyApiWrapper());

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = (apiWrapper, result) -> apiWrapper.setResult(result);
        (new TasksDispatcher(apiWrappers, serversPaths, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(apiWrappers.stream().map(apiWrapper -> apiWrapper.result().result)).contains(
            "I am first result from server 1",
            "I am second result from server 1"
        );
    }
}
