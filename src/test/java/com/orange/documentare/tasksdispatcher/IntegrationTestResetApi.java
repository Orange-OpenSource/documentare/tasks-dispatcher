package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.POST;

public interface IntegrationTestResetApi {
    @POST("/reset")
    Observable<Response<Void>> integrationTestReset();
}
