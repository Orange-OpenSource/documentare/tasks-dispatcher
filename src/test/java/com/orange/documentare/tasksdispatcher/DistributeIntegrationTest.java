package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled
@DisplayName("Distribute Integration test")
class DistributeIntegrationTest {

    private static final List<String> INTEGRATION_SERVER = Collections.singletonList("http://localhost:8080/");

    @BeforeEach
    void integrationTestReset() {
        new Retrofit.Builder()
            .baseUrl(INTEGRATION_SERVER.get(0))
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .build()
            .create(IntegrationTestResetApi.class)
            .integrationTestReset()
            .blockingLast();
    }

    @Test
    @DisplayName("Receive a task id and then a result")
    void receive_task_id_and_then_a_result() {
        // given

        MyApiWrapper myApiWrapper = new MyApiWrapper();
        List<ApiWrapper> apiWrappers = Collections.singletonList(myApiWrapper);

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = MyApiWrapper::setResult;
        (new TasksDispatcher(apiWrappers, INTEGRATION_SERVER, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(myApiWrapper.result().result).isEqualTo("result 1");
    }

    @Test
    @DisplayName("distribute many tasks, with random integration server1 errors")
    void distribute_many_tasks() {
        // given
        int count = 500;
        List<MyApiWrapper> apiWrappers = new ArrayList<>();
        IntStream.range(0, count).forEach(index -> apiWrappers.add(new MyApiWrapper()));

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = MyApiWrapper::setResult;
        (new TasksDispatcher(apiWrappers, INTEGRATION_SERVER, resultObserver))
            .distribute()
            .block();

        // then
        IntStream.range(0, count).forEach(index -> {
                MyApiWrapper apiWrapper = apiWrappers.get(index);
                assertThat(apiWrapper.result().result).isEqualTo("result " + apiWrapper.taskId().get());
            }
        );
    }
}