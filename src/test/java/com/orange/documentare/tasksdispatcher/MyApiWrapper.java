package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import io.reactivex.Observable;
import retrofit2.Response;

import java.util.UUID;

class MyApiWrapper extends ApiWrapper<MyApi, Result> {
    private final String requestId = UUID.randomUUID().toString();

    private Result result;

    @Override
    public Observable<RemoteTask> callComputationApi(String url) {
        return buildApi(MyApi.class, url).computationApi();
    }

    @Override
    public Observable<Response<Result>> callTaskApi(String taskId, String url) {
        return buildApi(MyApi.class, url).taskApi(taskId);
    }

    @Override
    public String requestId() {
        return requestId;
    }

    public void setResult(Result result) {
        this.result = result;
    }
    public Result result() {
        return result;
    }
}
