package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled
@DisplayName("Distribute On Two Machines Integration test")
class DistributeOnTwoMachinesIntegrationTest {

    private static final String INTEGRATION_SERVER_1 = "http://localhost:8080/";
    private static final String INTEGRATION_SERVER_2 = "http://localhost:9090/";
    private static final List<String> SERVERS = Arrays.asList(INTEGRATION_SERVER_1, INTEGRATION_SERVER_2);

    @BeforeEach
    void integrationTestReset() {
        SERVERS.forEach(server ->
            new Retrofit.Builder()
                .baseUrl(server)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()
                .create(IntegrationTestResetApi.class)
                .integrationTestReset()
                .blockingLast()
        );
    }

    @Test
    @DisplayName("Receive a task id and then a result")
    void receive_task_id_and_then_a_result() {
        // given
        List<MyApiWrapper> apiWrappers = Arrays.asList(new MyApiWrapper(), new MyApiWrapper());

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = MyApiWrapper::setResult;
        (new TasksDispatcher(apiWrappers, SERVERS, resultObserver))
            .distribute()
            .block();

        // then
        assertThat(apiWrappers.stream().map(apiWrapper -> apiWrapper.result().result)).containsExactly(
            "result 1", "result 1001"
        );
    }

    @Test
    @DisplayName("distribute many tasks, with random integration server1 errors")
    void distribute_many_tasks() {
        // given
        int count = 500;
        List<MyApiWrapper> apiWrappers = new ArrayList<>();
        IntStream.range(0, count).forEach(index -> apiWrappers.add(new MyApiWrapper()));

        // when
        BiConsumer<MyApiWrapper, Result> resultObserver = MyApiWrapper::setResult;
        (new TasksDispatcher(apiWrappers, SERVERS, resultObserver))
            .distribute()
            .block();

        // then
        IntStream.range(0, count).forEach(index -> {
                MyApiWrapper apiWrapper = apiWrappers.get(index);
                assertThat(apiWrapper.result().result).isEqualTo("result " + apiWrapper.taskId().get());
            }
        );
    }
}