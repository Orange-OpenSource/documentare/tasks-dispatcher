package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import io.reactivex.Observable;
import io.reactivex.subjects.ReplaySubject;
import lombok.extern.slf4j.Slf4j;
import retrofit2.HttpException;
import retrofit2.Response;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

@Slf4j
public class TasksDispatcher<API, RES> {

    private static final int RESTART_DELAY_MS = 1000;

    private final ReplaySubject<String> allTasksDone = ReplaySubject.create();
    private final ApiWrappersBucket<API, RES> apiWrappers;
    private final List<String> serversUrls;
    private final int expectedReceivedResults;
    private final BiConsumer<ApiWrapper<API, RES>, RES> resultObserver;
    private final AtomicInteger actualReceivedResults = new AtomicInteger();

    private int serversTaskDistributionIndex;

    public TasksDispatcher(Collection<ApiWrapper<API, RES>> apiWrappers, List<String> serversUrls, BiConsumer<ApiWrapper<API, RES>, RES> resultObserver) {
        this.apiWrappers = new ApiWrappersBucket<>(apiWrappers);
        this.serversUrls = serversUrls;
        this.resultObserver = resultObserver;
        this.expectedReceivedResults = apiWrappers.size();
    }

    public TasksDispatcher distribute() {
        distributeANewTask();
        return this;
    }

    private void distributeANewTask() {
        if (apiWrappers.hasNext()) {
            retrieveATaskId(apiWrappers.next(), nextServerPath());
        }
    }

    private void retrieveATaskId(ApiWrapper<API, RES> apiWrapper, String url) {
        log.info("[reqId '{}'; threadId '{}'; conn pool count '{}']: retrieve a task id on '{}'", apiWrapper.requestId(), Thread.currentThread().getId(), ApiWrapper.retrieveClient().connectionPool().connectionCount(), url);
        Observable<RemoteTask> observable = apiWrapper.callComputationApi(url);
        observable.subscribe(
            remoteTask -> receiveTaskId(remoteTask, apiWrapper, url),
            throwable -> taskIdError(throwable, apiWrapper, url)
        );
    }

    private void receiveTaskId(RemoteTask remoteTask, ApiWrapper<API, RES> apiWrapper, String url) {
        log.info("[reqId '{}']: taskId is '{}': threadId is '{}'", apiWrapper.requestId(), remoteTask.id, Thread.currentThread().getId());

        apiWrapper.setTaskId(remoteTask.id);
        requestResult(remoteTask, apiWrapper, url);

        distributeANewTask();
    }

    private void requestResult(RemoteTask remoteTask, ApiWrapper<API, RES> apiWrapper, String url) {
        log.info("[reqId '{}']: taskId '{}']: request result", apiWrapper.requestId(), remoteTask.id);
        Observable<Response<RES>> observable = apiWrapper.callTaskApi(remoteTask.id, url);
        observable.subscribe(
            response -> receiveResult(response, remoteTask, apiWrapper, url),
            throwable -> taskResultError(throwable, remoteTask, apiWrapper)
        );
    }

    private void taskIdError(Throwable throwable, ApiWrapper<API, RES> apiWrapper, String url) {
        boolean err503 = throwable instanceof HttpException && ((HttpException) throwable).code() == 503;
        if (err503) {
            log.info("[reqId '{}']: server at '{}' not available (503)", apiWrapper.requestId(), url);
        } else {
            log.error("[reqId '{}']: FAILED to get a task id", apiWrapper.requestId(), throwable);
        }
        tryAgainToDistributeTaskOnAnotherMachine(apiWrapper);
    }

    private void receiveResult(Response response, RemoteTask remoteTask, ApiWrapper<API, RES> apiWrapper, String url) {
        if (response.code() == 200) {
            log.info("[reqId '{}']: taskId '{}']: result received", apiWrapper.requestId(), remoteTask.id);
            Object result = response.body();
            resultObserver.accept(apiWrapper, (RES) result);
            if (expectedReceivedResults == actualReceivedResults.incrementAndGet()) {
                allTasksDone.onNext("done");
                allTasksDone.onComplete();
            }
            log.info("actualReceivedResults: " + actualReceivedResults.get());
            return;
        } else if (response.code() == 204) {
            log.info("[reqId '{}']: taskId '{}']: result not available yet; http status code: {}; response: {}", apiWrapper.requestId(), remoteTask.id, response.code(), response);
            tryAgainToReceiveResult(remoteTask, apiWrapper, url);
        } else {
            log.error("[reqId '{}']: taskId '{}']: an error occurred while retrieving result; http status code: {}; response: {}", apiWrapper.requestId(), remoteTask.id, response.code(), response);
            tryAgainToDistributeTaskOnAnotherMachine(apiWrapper);
        }
    }


    private void tryAgainToReceiveResult(RemoteTask remoteTask, ApiWrapper<API, RES> apiWrapper, String url) {
        Observable.just("restart since result was not ready")
            .delay(RESTART_DELAY_MS, TimeUnit.MILLISECONDS)
            .subscribe(param -> requestResult(remoteTask, apiWrapper, url));
    }

    private void tryAgainToDistributeTaskOnAnotherMachine(ApiWrapper<API, RES> apiWrapper) {
        log.info("[reqId '{}']: try to distribute task on another machine", apiWrapper.requestId());
        apiWrappers.addBack(apiWrapper);
        Observable.just("restart since a problem occurred")
            .delay(RESTART_DELAY_MS, TimeUnit.MILLISECONDS)
            .subscribe(param -> distributeANewTask());
    }

    private void taskResultError(Object throwable, RemoteTask remoteTask, ApiWrapper<API, RES> apiWrapper) {
        log.error("[reqId '{}']: taskId '{}']: FAILED to receive result; error '{}'", apiWrapper.requestId(), remoteTask.id, throwable);
        tryAgainToDistributeTaskOnAnotherMachine(apiWrapper);
    }

    private String nextServerPath() {
        String nextServerPath = serversUrls.get(serversTaskDistributionIndex);
        serversTaskDistributionIndex = (serversTaskDistributionIndex + 1) % serversUrls.size();
        return nextServerPath;
    }

    public void block() {
        allTasksDone.blockingLast();
    }
}
