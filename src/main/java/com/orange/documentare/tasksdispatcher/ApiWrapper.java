package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.lang.ref.SoftReference;
import java.util.Optional;

public abstract class ApiWrapper<API, RES> {
    private Optional<String> taskId;

    // reuse okhttp client, so share connection pool, etc
    // see https://stackoverflow.com/questions/42948985/okhttp-connection-pool-and-file-handles#42949077
    private static SoftReference<OkHttpClient> softReference;


    protected API buildApi(Class<API> clazz, String url) {
        OkHttpClient client = retrieveClient();
        return new Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(client)
            .build()
            .create(clazz);
    }

    static synchronized OkHttpClient retrieveClient() {
        if (softReference == null || softReference.get() == null) {
            OkHttpClient client = (new OkHttpClient.Builder()).build();
            softReference = new SoftReference<>(client);
        }
        return softReference.get();
    }

    public abstract Observable<RemoteTask> callComputationApi(String url);

    public abstract Observable<Response<RES>> callTaskApi(String taskId, String url);

    public void setTaskId(String taskId) {
        this.taskId = Optional.of(taskId);
    }

    public Optional<String> taskId() {
        return taskId;
    }

    public abstract String requestId();
}
