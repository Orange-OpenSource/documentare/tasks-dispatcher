package com.orange.documentare.tasksdispatcher;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class ApiWrappersBucket<API, RES> {
    private final List<ApiWrapper<API, RES>> apiWrappers;

    ApiWrappersBucket(Collection<ApiWrapper<API, RES>> apiWrappers) {
        this.apiWrappers = new ArrayList(apiWrappers);
    }

    synchronized boolean hasNext() {
        return !apiWrappers.isEmpty();
    }

    synchronized ApiWrapper<API, RES> next() {
        ApiWrapper apiWrapper = apiWrappers.get(0);
        apiWrappers.remove(apiWrapper);
        return apiWrapper;
    }

    synchronized void addBack(ApiWrapper<API, RES> apiWrapper) {
        apiWrappers.add(apiWrapper);
    }
}
