package com.example.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RestController
public class TaskController {

    public static final int MAX_NB_TASKS = 10;

    @Value("${firstTaskId}")
    int firstTaskId;

    @RequiredArgsConstructor
    public class RemoteTask {
        public final String id;
    }

    @RequiredArgsConstructor
    public static class Result {
        public final String result;
    }

    private AtomicInteger taskId;
    private AtomicInteger runningTasks = new AtomicInteger(0);
    private AtomicInteger errorTrigger = new AtomicInteger(0);

    @PostConstruct
    public void init() {
        taskId = new AtomicInteger(firstTaskId);
        log.info("firstTaskId = " + firstTaskId);
        log.info("taskId = " + taskId);
    }

    @RequestMapping("/do")
    public ResponseEntity<RemoteTask> provideComputationTaskId() {
        if (runningTasks.get() >= MAX_NB_TASKS) {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
        checkIfWeNeedToGenerateAnError();

        runningTasks.incrementAndGet();
        int tid = taskId.incrementAndGet();
        log.info("create task id " + tid);
        return new ResponseEntity<>(
            new RemoteTask(Integer.toString(tid)), HttpStatus.OK
        );
    }

    @RequestMapping("/task/{taskId}")
    public Result provideResultIfAvailable(@PathVariable int taskId) throws InterruptedException {
        checkIfWeNeedToGenerateAnError();

        log.info("return result for task id " + taskId);
        Thread.sleep(100);
        runningTasks.decrementAndGet();
        return new Result("result " + taskId);
    }

    private void checkIfWeNeedToGenerateAnError() {
        if (errorTrigger.incrementAndGet() > 10) {
            errorTrigger.set(0);
            throw new IllegalStateException("Simulate server error");
        }
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public void reset() {
        taskId.set(firstTaskId);
        runningTasks.set(0);
        log.info("reset");
    }
}
